package programa;
/**
 * @author AyoubSaif
 */
import java.util.Scanner;
import clases.Gimnasio;

public class Programa {

	static Scanner input = new Scanner(System.in);
	
	public static void main(String[] args) {
		
		Gimnasio gimnasio = new Gimnasio();
		
		int opt;
		
		do {
			System.out.println("\n\n###  PRACTICA 5 ###");
			System.out.println("\n Elige unna opci�n:\n");
			System.out.println("1. Crear un usuario nuevo");
			System.out.println("2. Listar usuarios");
			System.out.println("3. Buscar usuario");
			System.out.println("4. Eliminar un usuario");
			System.out.println("5. Crear un cliente nuevo");
			System.out.println("6. Mostrar lista de Clientes");
			System.out.println("7. Buscar un cliente");
			System.out.println("8. Eliminar un cliente");
			System.out.println("9. Listar cliente por el a�o de fecha de suscripci�n");
			System.out.println("10. Buscar usuarios por fecha de nacimiento ");
			System.out.println("11. Asignar usuario a un cliente");
			System.out.println("12. Listar clientes por dni de usuario");
			System.out.println("13. Ordenar usuarios por nombre");
			System.out.println("14.- Salir");
			opt = input.nextInt();

			switch (opt) {
			case 1:
				gimnasio.altaUsuario();
				break;
			case 2:
				gimnasio.listaUsuarios();
				break;
			case 3:
				System.out.println(gimnasio.buscarUsuario());
				break;
			case 4:
				gimnasio.eliminarUsuario();
				break;
			case 5:
				gimnasio.altaCliente();
				break;
			case 6:
				gimnasio.listarClientes();
				break;
			case 7:
				System.out.println(gimnasio.buscarCliente());
				break;
			case 8:
				gimnasio.eliminarCliente();
				break;
			case 9:
				gimnasio.listarClienteSub();
				break;
			case 10:
				gimnasio.listarUsuarioEdad();
				break;
			case 11:
				gimnasio.asignarUsuario();
				break;	
			case 12:
				gimnasio.listarClientePorDniUsuario();
				break;
			case 13:
				gimnasio.ordenarUsuarios();
				break;
			case 14:
				System.out.println("Fin del Programa");
				System.exit(0);
				break;
			default:
				System.out.println("Opci�n incorrecta o no establecida dentro del rango, "
						+ "introduzca un numero entre 1 y 14.");
				
			}

		} while (opt != 14);

		input.close();
	}

}
