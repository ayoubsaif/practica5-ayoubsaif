package clases;
/**
 * @autor AyoubSaif
 */
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.Scanner;

public class Gimnasio {

		/**
		 * Declaracion e inicializacion de la clase Scanner
		 */
		static Scanner input = new Scanner(System.in);
		
		/**
		 * Declaracion de Arraylist de Usuario y de Cliente
		 */
		private ArrayList<Usuario> listaUsuarios;
		private ArrayList<Cliente> listaClientes;
		
		/**
		 * Constructor Arraylists de Usuario y de Cliente
		 */
		public Gimnasio() {
			listaUsuarios = new ArrayList<Usuario>();
			listaClientes = new ArrayList<Cliente>();

		}
		/**
		 * M�todo que comprueba si existe alg�n Usuario
		 * 
		 * @param dni
		 * 
		 * @return valor verdadero o falso
		 */
		public boolean existeUsuario(String dni) {
			for (Usuario usuario : listaUsuarios) {
				if (usuario != null && usuario.getDni().equals(dni)) {
					return true;
				}
			}
			return false;

		}
		/**
		 * M�todo para dar de alta un usuario nuevo, si el dni del usuario ya existe, no lo
		 * creara y mostrar� que usuario existe con ese dni.
		 * 
		 */

		
		public void altaUsuario() {
			System.out.println("Introduzca el dni:");
			String dni = input.nextLine();
			if (!existeUsuario(dni)) {
				Usuario nuevoUsuario = new Usuario();
				nuevoUsuario.setDni(dni);
				System.out.println("Introduce el nombre:");
				String nombre = input.nextLine();
				nuevoUsuario.setNombre(nombre);
				System.out.println("Introduce el apellido:");
				String apellido = input.nextLine();
				nuevoUsuario.setApellido(apellido);
				System.out.println("Introduce la edad del usuario:");
				int edad = input.nextInt();
				nuevoUsuario.setEdad(edad);
				input.nextLine();
				listaUsuarios.add(nuevoUsuario);
				System.out.println("\n");
				System.out.println("Usuario Creado.");
			} else {
				System.out.println("El usuario ya existe con el siguiente dni: "+dni);
			}
		}
		
		/**
		 * M�todo para listar usuarios
		 * 
		 */
		public void listaUsuarios() {
			if (listaUsuarios.size()>=0) {
				for (Usuario usuario : listaUsuarios) {
					if (usuario != null) {
						System.out.println(usuario);
					}
				}
			} else {
				System.out.println("No existe ning�n usuario creado");
			}
		}
		

		/**
		 * M�todo para buscar un usuario, si no existe ningun usuario con el nombre
		 * devuelvera null.
		 * 
		 * @return Objeto Usuario
		 */
		public Usuario buscarUsuario() {
			System.out.println("Introduzca el dni del usuario:");
			String dniUsuario = input.nextLine().toLowerCase();
			for (Usuario usuario : listaUsuarios) {
				if (usuario != null && usuario.getDni().equals(dniUsuario)) {
					return usuario;
				}
			}
			System.out.println("No existe ning�n usuario con ese dni");
			return null;
		}
		
		/**
		 * M�todo para eliminar un usuario por el nombre si existe.
		 * 
		 */
		public void eliminarUsuario() {
			System.out.println("Introduce en nombre del usuario a eliminar:");
			String nombreUsuario = input.nextLine().toLowerCase();
			if (existeUsuario(nombreUsuario)) {
				Iterator<Usuario> iteradorUsuarios = listaUsuarios.iterator();
				while (iteradorUsuarios.hasNext()) {
					Usuario usuario = iteradorUsuarios.next();
					if (usuario.getNombre().equals(nombreUsuario)) {
						iteradorUsuarios.remove();
					}
				}
				System.out.println("Usuario eliminado.");
			} else {
				System.out.println("No existe ningun usuario con este nombre");
			}
		}
		
		/**
		 * M�todo que comprueba si existe alg�n Cliente por nombre.
		 * 
		 * @param nombre del cliente
		 * 
		 * @return valor verdadero o falso
		 */
		public boolean existeCliente(String nombre) {
			for (Cliente cliente : listaClientes) {
				if (cliente != null && cliente.getNombre().equals(nombre)) {
					return true;
				}
			}
			return false;
		}
		/**
		 * M�todo para dar de alta un Cliente, si el nombre del Cliente ya existe, no lo
		 * creara.
		 */
		public void altaCliente() {
			System.out.println("Introduzca el nombre del Cliente:");
			String nombreCliente = input.nextLine();
			if (!existeCliente(nombreCliente)) {
				Cliente nuevoCliente = new Cliente();
				nuevoCliente.setNombre(nombreCliente);
				System.out.println("Introduce el tipo de Subscripci�n:");
				System.out.println("1. Basic \n 2. Premium \n 3. SuperPremium");
				int tiposub = input.nextInt();
				nuevoCliente.setTipoSub(tiposub);
				System.out.println("Introduce el precio:");
				double precio = input.nextDouble();
				input.nextLine();
				nuevoCliente.setPrecio(precio);
				nuevoCliente.setFechaSub(LocalDate.now());
				listaClientes.add(nuevoCliente);
				System.out.println("\n �Cliente creado correctamente!");

			} else {
				System.out.println("El cliente ya existe.");
			}
		}
		
		/**
		 * M�todo para listar clientes
		 * 
		 */
		public void listarClientes() {
			if (listaClientes.size() > 0) {
				for (Cliente cliente : listaClientes) {
					if (cliente != null) {
						System.out.println(cliente);
					}
				}
			} else {
				System.out.println("No hay ning�n cliente registrado");
			}
		}
		/**
		 * M�todo para buscar un cliente, si no existe ningun cliente por nombre,
		 * devuelve null.
		 * 
		 * @return Objeto Cliente
		 */
		public Cliente buscarCliente() {
			System.out.println("Introduce el nombre del cliente:");
			String nombreCliente = input.nextLine().toLowerCase();
			for (Cliente cliente : listaClientes) {
				if (cliente != null && cliente.getNombre().equals(nombreCliente)) {
					return cliente;
				}
			}
			System.out.println("No existe ning�n cliente con ese nombre");
			return null;
		}
		/**
		 * M�todo para eliminar un cliente buscando por nombre.
		 * 
		 */
		public void eliminarCliente() {
			System.out.println("Introduzca el nombre del cliente a eliminar:");
			String nombreCliente = input.nextLine().toLowerCase();
			if (existeCliente(nombreCliente)) {
				Iterator<Cliente> iteradorClientes = listaClientes.iterator();
				while (iteradorClientes.hasNext()) {
					Cliente cliente = iteradorClientes.next();
					if (cliente.getNombre().equals(nombreCliente)) {
						iteradorClientes.remove();
					}
				}
				System.out.println("Cliente eliminado.");
			} else {
				System.out.println("�El nombre" + nombreCliente + " no existe como Cliente!");
			}
		}
		/**
		 * M�todo para listar clientes por a�o de suscripci�n al Gimnasio
		 * 
		 */
		public void listarClienteSub() {
			System.out.println("Introduce el a�o de subscripcion a buscar:");
			int fechaSub = input.nextInt();
			input.nextLine();
			if (listaClientes.size() > 0) {
				for (Cliente clientes : listaClientes) {
					if (clientes.getFechaSub().getYear() == fechaSub) {
						System.out.println(clientes);
					}
				}
			} else {
				System.out.println("No hay ning�n cliente registrado en el a�o " + fechaSub);
			}
		}
		/**
		 * M�todo para asignar un cliente a un usuario
		 * 
		 */
		public void asignarUsuario() {
			Usuario usuario = buscarUsuario();
			if (usuario != null) {
				Cliente cliente = buscarCliente();
				if (cliente != null) {
					cliente.setUsuario(usuario);
					System.out.println("");
					System.out.println("Usuario asignado");
				}
			}
		}

		/**
		 * M�todo para listar clientes por dni de usuario
		 * 
		 */
		public void listarClientePorDniUsuario() {
			Usuario usuario = buscarUsuario();
			if (usuario != null) {
				for (Cliente cliente : listaClientes) {
					if (cliente.getUsuario().equals(usuario)) {
						System.out.println(cliente);
					} else {
						System.out.println("El usuario no tiene asignado ning�n cliente");
					}
				}
			}
		}
		
		/**
		 * M�todo para ordenar usuario
		 */
		public void ordenarUsuarios() {
			for (int i = 0; i < listaUsuarios.size() - 1; i++) {
				for (int j = i + 1; j < listaUsuarios.size(); j++) {
					if (listaUsuarios.get(i).getNombre().compareTo(listaUsuarios.get(j).getNombre()) > 0) {
						Usuario tmp = listaUsuarios.get(i);
						listaUsuarios.set(i, listaUsuarios.get(j));
						listaUsuarios.set(j, tmp);
					}
				}
			}
			System.out.println("Se ha ordenado la lista de usuarios por Nombre.");
		}
		
		/**
		 * M�todo para listar usuarios por edad
		 * 
		 */
		public void listarUsuarioEdad() {
			System.out.println("Introduce la edad a buscar:");
			int edad = input.nextInt();
			input.nextLine();
			if (listaUsuarios.size() > 0) {
				for (Usuario usuarios : listaUsuarios) {
					if (usuarios.getEdad() == edad) {
						System.out.println(usuarios);
					}
				}
			} else {
				System.out.println("No hay ning�n usuario con la edad de " + edad + " a�os.");
			}
		}
}
