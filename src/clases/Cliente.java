package clases;

/**
 * @author AyoubSaif
 * 
 */
import java.time.LocalDate;

public class Cliente {
	/**
	 * Atributos
	 */
	private String nombre;
	private int tipoSub;
	private double precio;
	private LocalDate fechaSub;
	private Usuario usuario;
	
	/**
	 * Constructor
	 * 
	 */
	public Cliente() {
		this.nombre= "";
		this.tipoSub = 0;
		this.precio = 0;
	}
	
	public String getNombre() {
		return nombre;
	}
	/**
	 * 
	 * @param nombre
	 */
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}
	public int getTipoSub() {
		return tipoSub;
	}
	public void setTipoSub(int tipoSub) {
		this.tipoSub = tipoSub;
	}
	public double getPrecio() {
		return precio;
	}
	public void setPrecio(double precio) {
		this.precio = precio;
	}
	public LocalDate getFechaSub() {
		return fechaSub;
	}
	public void setFechaSub(LocalDate fechaSub) {
		this.fechaSub = fechaSub;
	}

	public Usuario getUsuario() {
		return usuario;
	}
	public void setUsuario(Usuario usuario) {
		this.usuario = usuario;
	}

	/**
	 * toString
	 */
	@Override
	public String toString() {
		return "Cliente nombre:" + nombre + ", tipoSub:" + tipoSub + ", precio:" + precio + ", fechaSub:" + fechaSub + ", usuario:" + usuario + "\n";
	}
	
}
